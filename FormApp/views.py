from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Product


def addProduct(request):
    try:
        product = Product.objects.get(Name=request.POST['Name'])
    except:
        product = Product(Name=request.POST['Name'])
    product.old_price = product.price
    product.price = request.POST['prix']
    product.save()

    return redirect('FormApp:product_index')

class listProducts(ListView):
    model = Product
    context_object_name = "products"
    template_name = "FormApp/index.html"
    paginate_by = 3
    queryset = Product.objects.all().order_by('price')


# Create your views here.
