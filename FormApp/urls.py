from django.urls import path

from . import views
from .models import Product

app_name = 'FormApp'

urlpatterns = [
    path('', views.listProducts.as_view(), name='product_index'),
    path('add', views.addProduct, name='product_add'),
]