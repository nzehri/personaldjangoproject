from django.db import models

class Product(models.Model):
    Name = models.CharField(max_length=100)
    price = models.IntegerField(default=0)
    old_price = models.IntegerField(null=True)

    def __str__(self):
        return self.Name

# Create your models here.
