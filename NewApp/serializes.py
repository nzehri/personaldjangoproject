from rest_framework import serializers
from .models import Article

class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    author = serializers.CharField(max_length=42)
    body = serializers.CharField()
    date = serializers.DateTimeField()

    def create(self, validated_data):
        
        return Article.objects.create(**validated_data)