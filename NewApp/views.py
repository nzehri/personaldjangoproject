from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404
from django.core.mail import send_mail
from NewApp.models import Article, Comment
from .forms import ArticleForm, CommentForm
from .serializes import ArticleSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
from django.views.decorators.cache import cache_page

@cache_page(60 * 2)

def index(request):
    articles = Article.objects.all()

    return render(request, 'NewApp/accueil.html', {'articles': articles})

def showArticle(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    comments = article.comment_set.all()

    return render(request, 'NewApp/article.html', {'article': article, 'comments': comments})

def addArticle(request):

    article = Article()
    article.title = request.POST['title']
    article.body = request.POST['body']
    article.author = request.POST['author']
    article.save()

    return redirect('NewApp:index')

def addComment(request, article_id):

    article = get_object_or_404(Article, pk=article_id)
    comment = Comment()
    comment.article = article
    comment.body = request.POST['body']
    comment.save()
    return redirect ('NewApp:article', article_id)

def send(request):
    send_mail('Hello', f'Hello there', 'zehri.n2013@gmail.com', ['nassim.zhr@gmail.com'], fail_silently=False)

    return render(request, 'NewApp/sendEmail.html')

@csrf_exempt
def api_index(request):
    if request.method =='GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        
        data = JSONParser().parse(request)
        serializer = ArticleSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors, status=400)       