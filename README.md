This project contains 3 applications:

polls: small application for questions and answers

    urls:
        'polls/' list all questions
        'polls/<question_id>' show specific question
        'polls/<question_id>/results' show poll results for a question
        'polls/<question_id>/vote' participate to the poll

NewApp: small shop with rest api

    urls:
        'articles/' list all articles with comments made on every article
        'articles/<article_id>' show article with it's comments
        'articles/add' add new article
        'articles/<article_id>/addComment' add a comment on an article
        'articles/api' rest api with cache and cookies(depending on the method):
                    GET: list all articles
                    POST: add an article
                    PUT: update an article
                    DELETE: delete an article

FormApp: another small shop :p, using TemplateView, and ListView

    urls:
        'product/' show all products
        'product/add' use a form to add a product

